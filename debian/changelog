ruby-prometheus-client-mmap (1.1.1-1) UNRELEASED; urgency=medium

  * New upstream version
  * Reorder sequence of d/control fields by cme (routine-update)
  * Rules-Requires-Root: no (routine-update)

 -- Pirate Praveen <praveen@debian.org>  Sun, 04 Feb 2024 20:15:37 +0530

ruby-prometheus-client-mmap (0.23.1-3) unstable; urgency=medium

  * Reupload to unstable

 -- Pirate Praveen <praveen@debian.org>  Wed, 12 Jul 2023 00:30:00 +0530

ruby-prometheus-client-mmap (0.23.1-2) experimental; urgency=medium

  * Update copyright for vendor/c/{hashmap,jsmn}

 -- Pirate Praveen <praveen@debian.org>  Sat, 08 Jul 2023 16:56:32 +0530

ruby-prometheus-client-mmap (0.23.1-1) experimental; urgency=medium

  * New upstream version 0.23.1
  * Add ruby-rb-sys as build dependency

 -- Pirate Praveen <praveen@debian.org>  Fri, 07 Jul 2023 17:46:38 +0530

ruby-prometheus-client-mmap (0.19.1-2) unstable; urgency=medium

  * Reupload to unstable
  * Drop obsolete X*-Ruby-Versions fields

 -- Pirate Praveen <praveen@debian.org>  Sun, 11 Jun 2023 15:05:11 +0530

ruby-prometheus-client-mmap (0.19.1-1) experimental; urgency=medium

  * New upstream version 0.19.1
  * Enable tests that were disabled earlier due to segfaults

 -- Pirate Praveen <praveen@debian.org>  Mon, 17 Apr 2023 20:03:45 +0530

ruby-prometheus-client-mmap (0.17.0+spec-1) unstable; urgency=medium

  * Reupload to unstable
  * Switch to using gitlab.com tags
  * New upstream version 0.17.0+spec
  * Enable running upstream tests
  * Use gem install layout of dh ruby
  * Remove relative path to lib from spec in tests

 -- Pirate Praveen <praveen@debian.org>  Mon, 17 Apr 2023 16:43:46 +0530

ruby-prometheus-client-mmap (0.17.0-1) experimental; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + ruby-prometheus-client-mmap: Add
    Multi-Arch: same.

  [ Pirate Praveen ]
  * New upstream version 0.17.0
  * Bump Standards-Version to 4.6.2 (no changes needed)

 -- Pirate Praveen <praveen@debian.org>  Wed, 01 Mar 2023 18:12:31 +0530

ruby-prometheus-client-mmap (0.16.2-1) unstable; urgency=medium

  * New upstream version 0.16.2
  * Bump Standards-Version to 4.6.1 (no changes needed)

 -- Pirate Praveen <praveen@debian.org>  Fri, 19 Aug 2022 13:12:48 +0530

ruby-prometheus-client-mmap (0.15.0-2) unstable; urgency=medium

  * Reupload to unstable

 -- Pirate Praveen <praveen@debian.org>  Sat, 20 Nov 2021 23:37:04 +0530

ruby-prometheus-client-mmap (0.15.0-1) experimental; urgency=medium

  * New upstream version 0.15.0
  * Use ${ruby:Depends} for ruby dependencies

 -- Pirate Praveen <praveen@debian.org>  Wed, 27 Oct 2021 00:54:26 +0530

ruby-prometheus-client-mmap (0.12.0-2) unstable; urgency=medium

  * Reupload to unstable
  * Bump Standards-Version to 4.6.0 (no changes needed)
  * Bump debhelper compatibility level to 13
  * Update watch file standard

 -- Pirate Praveen <praveen@debian.org>  Sat, 28 Aug 2021 18:07:21 +0530

ruby-prometheus-client-mmap (0.12.0-1) experimental; urgency=medium

  * New upstream version 0.12.0
  * Bump Standards-Version to 4.5.1 (no changes needed)

 -- Pirate Praveen <praveen@debian.org>  Sun, 03 Jan 2021 16:05:54 +0530

ruby-prometheus-client-mmap (0.11.0-1) unstable; urgency=medium

  [ Cédric Boutillier ]
  * Update team name
  * Add .gitattributes to keep unwanted files out of the source package

  [ Pirate Praveen ]
  * New upstream version 0.11.0

 -- Pirate Praveen <praveen@debian.org>  Sun, 25 Oct 2020 00:57:14 +0530

ruby-prometheus-client-mmap (0.10.0-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Use secure copyright file specification URI.
  * Bump debhelper from old 11 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Pirate Praveen ]
  * Bump minimum version of gem2deb to 1.0~
  * Bump Standards-Version to 4.5.0 (no changes needed)

 -- Pirate Praveen <praveen@debian.org>  Tue, 14 Jul 2020 00:24:14 +0530

ruby-prometheus-client-mmap (0.10.0-1) unstable; urgency=medium

  * Team upload
  * New upstream version 0.10.0

 -- Sruthi Chandran <srud@debian.org>  Sun, 12 Jan 2020 00:51:05 +0530

ruby-prometheus-client-mmap (0.9.10-1) unstable; urgency=medium

  * Team upload

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

  [ Sruthi Chandran ]
  * New upstream version 0.9.10

 -- Sruthi Chandran <srud@debian.org>  Thu, 03 Oct 2019 22:39:31 +0530

ruby-prometheus-client-mmap (0.9.4-1) unstable; urgency=medium

  * New upstream version 0.9.4
  * Bump Standards-Version to 4.2.1 (no changes needed)
  * Move debian/watch to gemwatch.debian.net

 -- Pirate Praveen <praveen@debian.org>  Mon, 15 Oct 2018 17:04:17 +0530

ruby-prometheus-client-mmap (0.9.1-1) unstable; urgency=medium

  * New upstream version 0.9.1
  * Bump debhelper compat to 11 and standards version to 4.1.3

 -- Pirate Praveen <praveen@debian.org>  Sun, 18 Mar 2018 21:49:43 +0530

ruby-prometheus-client-mmap (0.7.0~beta14-1) unstable; urgency=medium

  * Initial release (Closes: #876456)

 -- Pirate Praveen <praveen@debian.org>  Thu, 02 Nov 2017 14:58:32 +0530
